﻿using System;

using TMCore.Enums;

namespace TMCore.Extensions
{
    public static class Extensions
    {
        public static string GetFriendlyString(this StepDirection dir)
        {
            switch (dir)
            {
                case StepDirection.Left:
                    return "Left";
                case StepDirection.Right:
                    return "Right";
                default:
                    return "Still";
            }
        }

        public static StepDirection ToStepDirection(this string dirName)
        {
            switch (dirName)
            {
                case "Left":
                    return StepDirection.Left;
                case "Right":
                    return StepDirection.Right;
                case "Still":
                    return StepDirection.Still;
                default:
                    throw new Exception("Step direction with given name doesn't exist");
            }
        }
    }
}
