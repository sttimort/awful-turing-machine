﻿using System;

using TMCore.Interfaces;

namespace TMCore.EventArguments
{
    public class StateChangedEventArgs : EventArgs
    {
        public readonly ITMState NewState;

        public StateChangedEventArgs(ITMState newState)
        {
            NewState = newState;
        }
    }
}
