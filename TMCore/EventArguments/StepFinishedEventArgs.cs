﻿using System;

using TMCore.Interfaces;

namespace TMCore.EventArguments
{
    public class StepFinishedEventArgs : EventArgs
    {
        public readonly int StepOrdinal;
        public readonly ITMStatement ExecutedStatement;
        public readonly ITapeRepresentation TapeRepresentation;

        public StepFinishedEventArgs(int StepOrdinal,
                                     ITMStatement ExecutedStatement,
                                     ITapeRepresentation TapeRepresentation)
        {
            this.StepOrdinal = StepOrdinal;
            this.ExecutedStatement = ExecutedStatement;
            this.TapeRepresentation = TapeRepresentation;
        }
    }
}
