﻿using System;
using System.Collections.Generic;

namespace TMCore.Interfaces
{
    public interface ITMProgram
    {
        ITMState InitialState { get; set; }
        ITMState FinalState { get; set; }
		bool IsReady { get; }

        IList<ITMStatement> Statements { get; }

		ITMStatement StatementForOldStateAndSymbol(ITMState oldState, char oldSymbol);

        ITMProgram GetCopy();

        event EventHandler InitialStateChanged;
    }
}
