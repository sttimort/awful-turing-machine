﻿using TMCore.Enums;

namespace TMCore.Interfaces
{
    public interface ITMStatement
    {
        ITMState OldState { get; }
        char OldSymbol { get; set; }

        ITMState NewState { get; set; }
        char NewSymbol { get; set; }
        StepDirection Direction { get; set; }
    }
}
