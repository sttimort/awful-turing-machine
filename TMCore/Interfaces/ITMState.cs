﻿namespace TMCore.Interfaces
{
    public interface ITMState
    {
        string Name { get; set; }
    }
}
