﻿using TMCore.Interfaces;
using TMCore.Enums;
using TMCore.Extensions;

namespace TMCore
{
    public struct TMStatement : ITMStatement
    {
        public TMStatement(ITMState OldState, char OldSymbol, ITMState NewState,
                           char NewSymbol, StepDirection Direction)
        {
            this.OldState = OldState;
            this.OldSymbol = OldSymbol;
            this.NewState = NewState;
            this.NewSymbol = NewSymbol;
            this.Direction = Direction;
        }

        public ITMState OldState { get; set; }
        public char OldSymbol { get; set; }
        public ITMState NewState { get; set; }
        public char NewSymbol { get; set; }
        public StepDirection Direction { get; set; }

        public override string ToString()
        {
            return string.Format("<{0}>{1}<{2}>{3}{4}", OldState, OldSymbol, NewState, NewSymbol, Direction.GetFriendlyString());
        }
    }
}
