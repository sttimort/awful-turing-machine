﻿using System;
using System.Collections.Generic;

using TMCore.Interfaces;

namespace TMCore
{
    public class TapeRepresentation : ITapeRepresentation
    {
        public TapeRepresentation(int headPosition, IList<char> cells)
        {
            HeadPosition = headPosition;
            Cells = cells;
        }

        public int HeadPosition { get; } // starting from 0

        public IList<char> Cells { get; }


        public override string ToString()
        {
            string repr = "";

            for (int i = 0; i < Cells.Count; i++)
            {
                if (i == HeadPosition)
                    repr += String.Format("[{0}]", Cells[i]);
                else
                    repr += Cells[i];
            }

            return repr;
        }
    }
}
