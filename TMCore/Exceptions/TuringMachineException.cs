﻿using System;
namespace TMCore.Exceptions
{
    public class TMachineException : Exception
    {
        public TMachineException(string msg = "Unspecified error")
            : base("Error: " + msg) { }
    }
}
