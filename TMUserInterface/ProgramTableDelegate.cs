﻿using System;

using AppKit;
using Foundation;

using TMCore;
using TMCore.Interfaces;
using TMUserInterface.Extensions;
using TMCore.Extensions;

namespace TMUserInterface
{
    public class ProgramTableDelegate : NSTableViewDelegate
    {
        TMStatesList _states;
        TMSymbols _symbols;

        MainViewController _simulator;

        public ProgramTableDelegate(TMSymbols symbols, TMStatesList states, MainViewController simulator)
        {
            _states = states;
            _symbols = symbols;

            _simulator = simulator;
        }

        public override NSView GetViewForItem(NSTableView tableView, NSTableColumn tableColumn, nint row)
        {
            NSView cell;

            if (tableColumn.Identifier == "StatesColumn")
            {
				cell = tableView.MakeView("StateNameCell", this);

                if (cell.Subviews.Find("StateNameLabel") is NSTextField StateNameLabel)
                {
                    StateNameLabel.StringValue = _states[(int)row].Name;
                    return cell;
                }

                throw new Exception();
            }

            var stateNameLabel = tableView.GetView(tableView.FindColumn((NSString)"StatesColumn"), row, false)
                              .Subviews.Find("StateNameLabel") as NSTextField;

            if (stateNameLabel == null)
                throw new Exception("State name label null");
            
            var stateName = stateNameLabel.StringValue;
            if (tableColumn.Title.Length != 1)
				throw new Exception("Symbol length is not 1");

            var symbol = tableColumn.Title[0];


            var stmt = _simulator._tm.Program.StatementForOldStateAndSymbol(_states[stateName], symbol);
            if (stmt == null)
            {
				cell = tableView.MakeView("EmptyStatementCell", this);
                cell.Identifier = "";

                var newStatementButton = cell.Subviews[0] as TMStatementAddButton;
                if (newStatementButton == null)
                    throw new Exception("New Statement Button doesn't exist");

                if (_simulator._tm.Program.FinalState.Name == stateName)
                    newStatementButton.OldState = _simulator._tm.Program.FinalState;
                else
                    newStatementButton.OldState = _states[stateName];
                
                newStatementButton.OldSymbol = symbol;

                newStatementButton.Activated += _simulator.OnNewStatementAction;
                return cell;
            }


            cell = tableView.MakeView("StatementCell", this);
            cell.Identifier = "";

            _ConfigureStatementCell(cell, stmt);

            return cell;
        }

        void _ConfigureStatementCell(NSView cell, ITMStatement stmt)
        {
			var NewStatePopUp = cell.Subviews.Find("NewStatePopUp") as TMPopUpButton;
			NewStatePopUp.Menu.RemoveAllItems();
			for (int i = 0; i < _states.Count; i++)
			{
				NewStatePopUp.Menu.AddItem(new NSMenuItem(_states[i].Name));
			}
            NewStatePopUp.Menu.AddItem(new NSMenuItem(_simulator._tm.Program.FinalState.Name));
			NewStatePopUp.SelectItem(stmt.NewState.Name);

            NewStatePopUp.OldState = stmt.OldState;
            NewStatePopUp.OldSymbol = stmt.OldSymbol;
            NewStatePopUp.Activated += _simulator.OnModifyStatementNewStateAction;


			var NewSymbolPopUp = cell.Subviews.Find("NewSymbolPopUp") as TMPopUpButton;
			NewSymbolPopUp.Menu.RemoveAllItems();
			for (int i = 0; i < _symbols.Count; i++)
			{
				NewSymbolPopUp.Menu.AddItem(new NSMenuItem(_symbols[i].ToString()));
			}
			NewSymbolPopUp.SelectItem(stmt.NewSymbol.ToString());

			NewSymbolPopUp.OldState = stmt.OldState;
			NewSymbolPopUp.OldSymbol = stmt.OldSymbol;
            NewSymbolPopUp.Activated += _simulator.OnModifyStatementNewSymbolAction;

			var DirectionPopUp = cell.Subviews.Find("DirectionPopUp") as TMPopUpButton;
			DirectionPopUp.SelectItem(stmt.Direction.GetFriendlyString());

            DirectionPopUp.OldState = stmt.OldState;
            DirectionPopUp.OldSymbol = stmt.OldSymbol;
            DirectionPopUp.Activated += _simulator.OnModifyStatementDirectionAction;
        }

        public override bool ShouldReorder(NSTableView tableView, nint columnIndex, nint newColumnIndex)
        {
            if (columnIndex == 0 || newColumnIndex == 0)
                return false;

            return true;
        }
    }
}
