﻿using System;

using AppKit;

namespace TMUserInterface
{
    public class TapeViewConroller
    {
        NSView[] _tape;

        public TapeViewConroller(NSStackView tapeStackView)
        {
            _tape = tapeStackView.ArrangedSubviews;
        }

        public void Fill(string tapeString)
        {
            if (tapeString.Length != 11)
                throw new InvalidOperationException("tapeString must be exactrly 11 characters");

            for (int i = 0; i < 11; i++)
            {
                var label = (NSTextField)_tape[i];
                label.StringValue = tapeString[i].ToString();
            }
        }
                
    }
}
