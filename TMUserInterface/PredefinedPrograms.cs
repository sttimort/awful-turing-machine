﻿using TMCore.Interfaces;
using TMCore.Enums;
using TMCore;

namespace TMUserInterface
{
    public static class PredefinedPrograms
    {
        public struct InputStrings
        {
            public static string Task1;
            public static string Task2;
            public static string Task3;
            public static string Task4;

            static InputStrings()
            {
                Task1 = "001122110";
                Task2 = "012012012";
                Task3 = "001122110";
                Task4 = "001122110";
            }
        }

        public static TMProgram EmptyProgram { get { return _CreateEmptyProgram(); }}
        public static TMProgram VerySimpleProgram { get { return _CreateVerySimpleProgram(); } }
        public static TMProgram Task1 => _CreateCreateTask1();
        public static TMProgram Task2 => _CreateCreateTask1();
        public static TMProgram Task3 => _CreateCreateTask3();
        public static TMProgram Task4 => _CreateCreateTask4();

        static TMProgram _CreateEmptyProgram()
        {
            return new TMProgram();
        }

		// replaces all 1s with 2s
        static TMProgram _CreateVerySimpleProgram()
        {
			var q0 = new TMState("q0");
			var q1 = new TMState("q1");

            var p = new TMProgram()
            {
                InitialState = q1,
                FinalState = q0
            };

			p.Statements.Add(new TMStatement(q1, '*', q0, '*', StepDirection.Still));
            p.Statements.Add(new TMStatement(q1, '1', q1, '2', StepDirection.Right));

            return p;
        }

		static TMProgram _CreateCreateTask1()
		{
			var q0 = new TMState("q0");
			var q1 = new TMState("q1");
            var q2 = new TMState("q2");
            var q3 = new TMState("q3");

            var p = new TMProgram()
            {
                InitialState = q1,
                FinalState = q0
            };

            p.Statements.Add(new TMStatement(q1, '*', q0, '1', StepDirection.Left));
            p.Statements.Add(new TMStatement(q1, '0', q3, '1', StepDirection.Right));
            p.Statements.Add(new TMStatement(q1, '1', q1, '2', StepDirection.Right));
            p.Statements.Add(new TMStatement(q1, '2', q3, '0', StepDirection.Right));

            p.Statements.Add(new TMStatement(q2, '*', q0, '2', StepDirection.Still));
            p.Statements.Add(new TMStatement(q2, '0', q2, '2', StepDirection.Right));
            p.Statements.Add(new TMStatement(q2, '1', q1, '0', StepDirection.Right));
            p.Statements.Add(new TMStatement(q2, '2', q2, '1', StepDirection.Right));

            p.Statements.Add(new TMStatement(q3, '*', q0, '0', StepDirection.Right));
            p.Statements.Add(new TMStatement(q3, '0', q2, '2', StepDirection.Left));
            p.Statements.Add(new TMStatement(q3, '1', q3, '2', StepDirection.Still));
            p.Statements.Add(new TMStatement(q3, '2', q1, '2', StepDirection.Left));
			return p;
		}

		static TMProgram _CreateCreateTask3()
		{
            var p = _CreateCreateTask1();

            foreach (var s in p.Statements)
                if (s.OldState.Name == "q2" && s.OldSymbol == '*') {
					s.NewState = s.OldState;
                    break;
                }
            
            return p;
		}

		static TMProgram _CreateCreateTask4()
		{
			var p = _CreateCreateTask3();

			foreach (var s in p.Statements)
				if (s.OldState.Name == "q2" && s.OldSymbol == '*')
				{
                    s.NewSymbol = '0';
                    s.Direction = StepDirection.Left;
					break;
				}

			return p;
		}
    }
}
