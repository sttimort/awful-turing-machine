// WARNING
//
// This file has been generated automatically by Visual Studio to store outlets and
// actions made in the UI designer. If it is removed, they will be lost.
// Manual changes to this file may not be handled correctly.
//
using Foundation;
using System.CodeDom.Compiler;

namespace TMUserInterface
{
	[Register ("ViewController")]
	partial class MainViewController
	{
		[Outlet]
		AppKit.NSButton AddNewStateButton { get; set; }

		[Outlet]
		AppKit.NSTextField AddNewStateField { get; set; }

		[Outlet]
		AppKit.NSButton AddNewSymbolButton { get; set; }

		[Outlet]
		AppKit.NSTextField AddNewSymbolField { get; set; }

		[Outlet]
		AppKit.NSButton ClearButton { get; set; }

		[Outlet]
		AppKit.NSButton ClearStatementsButton { get; set; }

		[Outlet]
		AppKit.NSTextField CurrentStateLabel { get; set; }

		[Outlet]
		AppKit.NSButton DelStateButton { get; set; }

		[Outlet]
		AppKit.NSTextField DelStateField { get; set; }

		[Outlet]
		AppKit.NSButton DelSymbolButton { get; set; }

		[Outlet]
		AppKit.NSTextField DelSymbolField { get; set; }

		[Outlet]
		AppKit.NSTextField FinalStateField { get; set; }

		[Outlet]
		AppKit.NSTextField InitialHeadPositionField { get; set; }

		[Outlet]
		AppKit.NSPopUpButton InitialStatePopUpButton { get; set; }

		[Outlet]
		AppKit.NSTextFieldCell InputStringField { get; set; }

		[Outlet]
		AppKit.NSTextField LogLabel { get; set; }

		[Outlet]
		AppKit.NSButton NextButton { get; set; }

		[Outlet]
		AppKit.NSButton PauseButton { get; set; }

		[Outlet]
		AppKit.NSTableView ProgramTableView { get; set; }

		[Outlet]
		AppKit.NSButton ResetButton { get; set; }

		[Outlet]
		AppKit.NSButton RunButton { get; set; }

		[Outlet]
		AppKit.NSTextField SimulatorStateLabel { get; set; }

		[Outlet]
		AppKit.NSSlider SpeedSliderControl { get; set; }

		[Outlet]
		AppKit.NSTextField StatusLabel { get; set; }

		[Outlet]
		AppKit.NSTextField StepsExecutedLabel { get; set; }

		[Outlet]
		AppKit.NSTextField StepsLimitField { get; set; }

		[Outlet]
		AppKit.NSStackView TapeStackView { get; set; }

		[Outlet]
		AppKit.NSPopUpButton ZeroSymbolPopUpButton { get; set; }

		[Action ("AddNewStateButtonAction:")]
		partial void AddNewStateButtonAction (Foundation.NSObject sender);

		[Action ("AddNewStateButtonClicked:")]
		partial void AddNewStateButtonClicked (Foundation.NSObject sender);

		[Action ("AddNewStateFieldAction:")]
		partial void AddNewStateFieldAction (Foundation.NSObject sender);

		[Action ("AddNewStateSubmit:")]
		partial void AddNewStateSubmit (Foundation.NSObject sender);

		[Action ("AddNewSymbolButtonAction:")]
		partial void AddNewSymbolButtonAction (Foundation.NSObject sender);

		[Action ("AddNewSymbolButtonClicked:")]
		partial void AddNewSymbolButtonClicked (Foundation.NSObject sender);

		[Action ("AddNewSymbolFieldAction:")]
		partial void AddNewSymbolFieldAction (Foundation.NSObject sender);

		[Action ("AddNewSymbolFieldSubmit:")]
		partial void AddNewSymbolFieldSubmit (Foundation.NSObject sender);

		[Action ("ClearStatementsButtonAction:")]
		partial void ClearStatementsButtonAction (Foundation.NSObject sender);

		[Action ("DelButtonAction:")]
		partial void DelButtonAction (Foundation.NSObject sender);

		[Action ("DelStateButtonAction:")]
		partial void DelStateButtonAction (Foundation.NSObject sender);

		[Action ("DelStateFieldAction:")]
		partial void DelStateFieldAction (Foundation.NSObject sender);

		[Action ("DelSymbolButtonAction:")]
		partial void DelSymbolButtonAction (Foundation.NSObject sender);

		[Action ("DelSymbolFieldAction:")]
		partial void DelSymbolFieldAction (Foundation.NSObject sender);

		[Action ("FinalStateFieldAction:")]
		partial void FinalStateFieldAction (Foundation.NSObject sender);

		[Action ("InitialHeadPositionFieldAction:")]
		partial void InitialHeadPositionFieldAction (Foundation.NSObject sender);

		[Action ("InitialStatePopUpButtonAction:")]
		partial void InitialStatePopUpButtonAction (Foundation.NSObject sender);

		[Action ("InputStringClearButtonAction:")]
		partial void InputStringClearButtonAction (Foundation.NSObject sender);

		[Action ("InputStringFieldAction:")]
		partial void InputStringFieldAction (Foundation.NSObject sender);

		[Action ("LoadButtonAction:")]
		partial void LoadButtonAction (Foundation.NSObject sender);

		[Action ("NextButtonAction:")]
		partial void NextButtonAction (Foundation.NSObject sender);

		[Action ("PauseButtonAction:")]
		partial void PauseButtonAction (Foundation.NSObject sender);

		[Action ("ResetButtonAction:")]
		partial void ResetButtonAction (Foundation.NSObject sender);

		[Action ("SpeedSlider:")]
		partial void SpeedSlider (Foundation.NSObject sender);

		[Action ("SpeedSliderAction:")]
		partial void SpeedSliderAction (Foundation.NSObject sender);

		[Action ("StartFromAction:")]
		partial void StartFromAction (Foundation.NSObject sender);

		[Action ("StepLimitFieldSubmit:")]
		partial void StepLimitFieldSubmit (Foundation.NSObject sender);

		[Action ("StepsLimitButtonAction:")]
		partial void StepsLimitButtonAction (Foundation.NSObject sender);

		[Action ("StepsLimitButtonClicked:")]
		partial void StepsLimitButtonClicked (Foundation.NSObject sender);

		[Action ("StepsLimitFieldAction:")]
		partial void StepsLimitFieldAction (Foundation.NSObject sender);

		[Action ("StepsLimitFieldSubmit:")]
		partial void StepsLimitFieldSubmit (Foundation.NSObject sender);

		[Action ("StopButtonAction:")]
		partial void StopButtonAction (Foundation.NSObject sender);

		[Action ("Task1ButtonAction:")]
		partial void Task1ButtonAction (Foundation.NSObject sender);

		[Action ("Task2ButtonAction:")]
		partial void Task2ButtonAction (Foundation.NSObject sender);

		[Action ("Task3ButtonAction:")]
		partial void Task3ButtonAction (Foundation.NSObject sender);

		[Action ("Task4ButtonAction:")]
		partial void Task4ButtonAction (Foundation.NSObject sender);

		[Action ("ZeroSymbolPopUpButtonAction:")]
		partial void ZeroSymbolPopUpButtonAction (Foundation.NSObject sender);
		
		void ReleaseDesignerOutlets ()
		{
			if (AddNewStateField != null) {
				AddNewStateField.Dispose ();
				AddNewStateField = null;
			}

			if (AddNewSymbolButton != null) {
				AddNewSymbolButton.Dispose ();
				AddNewSymbolButton = null;
			}

			if (ClearStatementsButton != null) {
				ClearStatementsButton.Dispose ();
				ClearStatementsButton = null;
			}

			if (ClearButton != null) {
				ClearButton.Dispose ();
				ClearButton = null;
			}

			if (InitialStatePopUpButton != null) {
				InitialStatePopUpButton.Dispose ();
				InitialStatePopUpButton = null;
			}

			if (DelStateField != null) {
				DelStateField.Dispose ();
				DelStateField = null;
			}

			if (DelSymbolButton != null) {
				DelSymbolButton.Dispose ();
				DelSymbolButton = null;
			}

			if (CurrentStateLabel != null) {
				CurrentStateLabel.Dispose ();
				CurrentStateLabel = null;
			}

			if (DelSymbolField != null) {
				DelSymbolField.Dispose ();
				DelSymbolField = null;
			}

			if (FinalStateField != null) {
				FinalStateField.Dispose ();
				FinalStateField = null;
			}

			if (InitialHeadPositionField != null) {
				InitialHeadPositionField.Dispose ();
				InitialHeadPositionField = null;
			}

			if (InputStringField != null) {
				InputStringField.Dispose ();
				InputStringField = null;
			}

			if (AddNewSymbolField != null) {
				AddNewSymbolField.Dispose ();
				AddNewSymbolField = null;
			}

			if (LogLabel != null) {
				LogLabel.Dispose ();
				LogLabel = null;
			}

			if (NextButton != null) {
				NextButton.Dispose ();
				NextButton = null;
			}

			if (PauseButton != null) {
				PauseButton.Dispose ();
				PauseButton = null;
			}

			if (AddNewStateButton != null) {
				AddNewStateButton.Dispose ();
				AddNewStateButton = null;
			}

			if (DelStateButton != null) {
				DelStateButton.Dispose ();
				DelStateButton = null;
			}

			if (ZeroSymbolPopUpButton != null) {
				ZeroSymbolPopUpButton.Dispose ();
				ZeroSymbolPopUpButton = null;
			}

			if (SpeedSliderControl != null) {
				SpeedSliderControl.Dispose ();
				SpeedSliderControl = null;
			}

			if (ProgramTableView != null) {
				ProgramTableView.Dispose ();
				ProgramTableView = null;
			}

			if (ResetButton != null) {
				ResetButton.Dispose ();
				ResetButton = null;
			}

			if (RunButton != null) {
				RunButton.Dispose ();
				RunButton = null;
			}

			if (SimulatorStateLabel != null) {
				SimulatorStateLabel.Dispose ();
				SimulatorStateLabel = null;
			}

			if (StatusLabel != null) {
				StatusLabel.Dispose ();
				StatusLabel = null;
			}

			if (StepsExecutedLabel != null) {
				StepsExecutedLabel.Dispose ();
				StepsExecutedLabel = null;
			}

			if (StepsLimitField != null) {
				StepsLimitField.Dispose ();
				StepsLimitField = null;
			}

			if (TapeStackView != null) {
				TapeStackView.Dispose ();
				TapeStackView = null;
			}
		}
	}
}
