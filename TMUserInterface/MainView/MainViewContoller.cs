﻿﻿using System;

using AppKit;
using Foundation;

using TMCore.Exceptions;
using TMCore;

using TMUserInterface.StatusMessages;
using TMUserInterface.Extensions;

namespace TMUserInterface
{
    public partial class MainViewController : NSViewController
    {
        enum StatusType { Success, Error }
        public enum TMSimulatorState { NotReady, Ready, Executing, Paused, Finished }

        struct TMSimulatorElementsStates
        {
            public bool InputStringOK;
            public bool InitialHeadPositionOK;
            public bool InputStringLoaded;
            public bool StepsLimitOK;
        }

        public readonly Machine _tm;

        //TapeViewConroller _tvc;
        //ProgramTableController _tpc;
        TMStatesList _states;
        TMSymbols _symbols;
        string _inputString;
        int _initialHeadPosition; // starting from 0
        int _stepsLimit;
        TMSimulatorState _simulatorStateField;
        TMSimulatorElementsStates _simulatorElementsStates;


        public MainViewController(IntPtr handle) : base(handle)
        {
            _inputString = string.Empty;
			_initialHeadPosition = 0; // first symbol
			_stepsLimit = 0; // 0 steps limit means unlimited
            _tm = new Machine(new Tape(), new TMProgram(), _inputString);
            _states = new TMStatesList(_tm.Program);
            _symbols = new TMSymbols(_tm.Program, _tm.ZeroCharacter);

            _simulatorStateField = TMSimulatorState.NotReady;
            _simulatorElementsStates = new TMSimulatorElementsStates
            {
                InputStringOK = true,
                InitialHeadPositionOK = true,
                InputStringLoaded = true,
                StepsLimitOK = true
            };


            _states.StatesListChanged += OnStatesListChanged;
            _symbols.AlphabetChanged += OnAlphabetChanged;
            _tm.StepFinished += OnStepFinished;
            _tm.FinishedRunning += OnFinishedRunning;
        }


        TMSimulatorState _simulatorState { 
            get { return _simulatorStateField; }
            set {
                _simulatorStateField = value;
                _AdjustUIToSimulatorState();
                SimulatorStateLabel.StringValue = _simulatorState.ToFriendlyString();
            }
        }

		public override NSObject RepresentedObject
		{
			get { return base.RepresentedObject; }
			set { base.RepresentedObject = value; }
		}



        public override void ViewDidLoad()
        {
            base.ViewDidLoad();

            InputStringField.StringValue = _inputString;
            InitialHeadPositionField.StringValue = (_initialHeadPosition+1).ToString();
            StepsLimitField.StringValue = _stepsLimit.ToString();

            InitialStatePopUpButton.Menu.RemoveAllItems();
            for (int i = 0; i < _states.Count; i++)
                InitialStatePopUpButton.Menu.AddItem(new NSMenuItem(_states[i].Name));
            FinalStateField.StringValue = _tm.Program.FinalState.Name;

            ZeroSymbolPopUpButton.Menu.RemoveAllItems();
			for (int i = 0; i < _symbols.Count; i++)
                ZeroSymbolPopUpButton.Menu.AddItem(new NSMenuItem(_symbols[i].ToString()));
            
            //_tvc = new TapeViewConroller(TapeStackView);

            _UpdateSimulatorState();
            _UpdateTMStateData();
            _AdjustUIToSimulatorState();

            _PrintStatus(SuccessStatusMessages.TMS_LOADED, StatusType.Success);
        }

        public override void AwakeFromNib()
        {
            base.AwakeFromNib();

            ProgramTableView.RowHeight = 67;

            foreach (var col in ProgramTableView.TableColumns())
                if (col.Identifier != "StatesColumn")
                    ProgramTableView.RemoveColumn(col);
            
            for (int i = 0; i < _symbols.Count; i++)
            {
                var col = new NSTableColumn("StatementsColumn");
                col.HeaderCell.Alignment = NSTextAlignment.Center;
                col.Width = 80;

                col.Title = _symbols[i].ToString();
                ProgramTableView.AddColumn(col);
            }

            ProgramTableView.DataSource = _states;
            ProgramTableView.Delegate = new ProgramTableDelegate(_symbols, _states, this);
        }


        void _LoadInputString(string inputString, int initialHeadPosition)
        {
            try {
                _tm.ResetWithInputString(inputString, initialHeadPosition);
            } catch (TMachineException e) {
                _PrintStatus(e.Message, StatusType.Error);
            }
        }


        void _UpdateTMStateData()
        {
            StepsExecutedLabel.StringValue = _tm.StepsExecuted.ToString();
            CurrentStateLabel.StringValue = _tm.CurrentState.Name;
        }

        void _PrintLineToLog(string s)
        {
            LogLabel.StringValue += s + "\n";
        }

        void _ClearLog()
        {
            LogLabel.StringValue = "";
        }

        void _PrintStatus(string msg, StatusType status)
        {
            if (status == StatusType.Error)
                StatusLabel.TextColor = NSColor.Red;
            else
                StatusLabel.TextColor = NSColor.FromRgb(68, 157, 68);

            StatusLabel.StringValue = msg;
        }

        void _LoadProgram(TMProgram p, string inputString = "", int initialHeadPosition = 1, int stepsLimit = 0)
        {
            if (inputString == null)
                throw new Exception("input string is null");

			_states.StatesListChanged -= OnStatesListChanged;
			_symbols.AlphabetChanged -= OnAlphabetChanged;

            _tm.Program = p;

            _states = new TMStatesList(p);
            _symbols = new TMSymbols(p, _tm.ZeroCharacter);
			_states.StatesListChanged += OnStatesListChanged;
			_symbols.AlphabetChanged += OnAlphabetChanged;

            InputStringField.StringValue = inputString;
            InitialHeadPositionField.StringValue = initialHeadPosition.ToString();
			StepsLimitField.StringValue = stepsLimit.ToString();


            InitialStatePopUpButton.Menu.RemoveAllItems();
            for (int i = 0; i < _states.Count; i++)
                InitialStatePopUpButton.Menu.AddItem(new NSMenuItem(_states[i].Name));
            FinalStateField.StringValue = _tm.Program.FinalState.Name;

            ZeroSymbolPopUpButton.Menu.RemoveAllItems();
            for (int i = 0; i < _symbols.Count; i++)
                ZeroSymbolPopUpButton.Menu.AddItem(new NSMenuItem(_symbols[i].ToString()));

            _UpdateSimulatorState();
            _UpdateTMStateData();
            _AdjustUIToSimulatorState();

			ProgramTableView.DataSource = _states;
			ProgramTableView.Delegate = new ProgramTableDelegate(_symbols, _states, this);
			foreach (var col in ProgramTableView.TableColumns())
				if (col.Identifier != "StatesColumn")
					ProgramTableView.RemoveColumn(col);

			for (int i = 0; i < _symbols.Count; i++)
			{
				var col = new NSTableColumn("StatementsColumn");
				col.HeaderCell.Alignment = NSTextAlignment.Center;
				col.Width = 80;

				col.Title = _symbols[i].ToString();
				ProgramTableView.AddColumn(col);
			}

            OnSetStepsLimit(StepsLimitField.StringValue);
            OnInputStringAction(InputStringField.StringValue);

			_PrintStatus(SuccessStatusMessages.TMS_LOADED, StatusType.Success);
        }
    }
}
