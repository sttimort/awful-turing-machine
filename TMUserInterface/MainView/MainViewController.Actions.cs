﻿using System;
using System.Text.RegularExpressions;

using AppKit;
using Foundation;

using TMCore.Enums;
using TMCore.Interfaces;
using TMCore.Exceptions;
using TMCore.Extensions;
using TMUserInterface.StatusMessages;

namespace TMUserInterface
{
    partial class MainViewController
    {
        partial void InputStringFieldAction(NSObject sender)
        {
            OnInputStringAction(InputStringField.StringValue);
        }

        partial void InputStringClearButtonAction(NSObject sender)
        {
            OnInputStringClearAction();
        }

        partial void InitialHeadPositionFieldAction(NSObject sender)
        {
            OnInitialHeadPositionAction(InitialHeadPositionField.StringValue);
        }



		[Action("RunButtonAction:")]
		async void RunButtonAction(NSObject sender)
		{
            if (_stepsLimit != 0 && _tm.StepsExecuted >= _stepsLimit)
                _PrintStatus(SuccessStatusMessages.STEPS_LIMIT_REACHED, StatusType.Success);

            else
            {
				_simulatorState = TMSimulatorState.Executing;
                try {
					await _tm.Run();
				}
				catch (TMachineException e) {
                    _PrintStatus(e.Message, StatusType.Error);
                    _simulatorState = TMSimulatorState.Paused;
				}
            }
        }

		partial void NextButtonAction(NSObject sender)
		{
        	if (_stepsLimit != 0 && _tm.StepsExecuted >= _stepsLimit)
				_PrintStatus(SuccessStatusMessages.STEPS_LIMIT_REACHED, StatusType.Success);

            else 
                try {
					_tm.Step();
	            }
	            catch (TMachineException e) {
                    _PrintStatus(e.Message, StatusType.Error);
				}

            if (_simulatorState != TMSimulatorState.Finished)
                _simulatorState = TMSimulatorState.Paused;
		}

		partial void PauseButtonAction(NSObject sender)
		{
            OnPause();
		}

		partial void ResetButtonAction(NSObject sender)
		{
            //OnPause();
            OnInputStringAction(_inputString);
		}

        // Turing Machine States Section ------------------------------
        partial void AddNewStateButtonAction(Foundation.NSObject sender)
        {
            OnAddNewState(AddNewStateField.StringValue);
        }

        partial void AddNewStateFieldAction(Foundation.NSObject sender)
        {
            OnAddNewState(AddNewStateField.StringValue);
        }

        partial void DelStateButtonAction(Foundation.NSObject sender)
        {
            OnDelState(DelStateField.StringValue);
        }

        partial void DelStateFieldAction(Foundation.NSObject sender)
        {
            OnDelState(DelStateField.StringValue);
        }

        partial void InitialStatePopUpButtonAction(Foundation.NSObject sender)
        {
            OnSetInitialState(InitialStatePopUpButton.SelectedItem.Title);
        }

        partial void FinalStateFieldAction(NSObject sender)
        {
            OnSetFinalState(FinalStateField.StringValue);
        }

        // Turing Machine Alphabet Section ----------------------------
        partial void AddNewSymbolFieldAction(Foundation.NSObject sender)
        {
            if (AddNewSymbolField.StringValue.Length != 1 || string.IsNullOrWhiteSpace(AddNewSymbolField.StringValue))
                _PrintStatus(ErrorStatusMessages.INVALID_SYMBOL, StatusType.Error);
            else
                OnAddNewSymbol(AddNewSymbolField.StringValue[0]);
        }

        partial void AddNewSymbolButtonAction(Foundation.NSObject sender)
        {
			if (AddNewSymbolField.StringValue.Length != 1 || string.IsNullOrWhiteSpace(AddNewSymbolField.StringValue))
				_PrintStatus(ErrorStatusMessages.INVALID_SYMBOL, StatusType.Error);
			else
				OnAddNewSymbol(AddNewSymbolField.StringValue[0]);
        }

        partial void DelSymbolFieldAction(Foundation.NSObject sender)
        {
            if (DelSymbolField.StringValue.Length != 1 || string.IsNullOrWhiteSpace(DelSymbolField.StringValue))
				_PrintStatus(ErrorStatusMessages.INVALID_SYMBOL, StatusType.Error);
			else
                OnDelSymbol(DelSymbolField.StringValue[0]);
        }

        partial void DelSymbolButtonAction(Foundation.NSObject sender)
        {
			if (DelSymbolField.StringValue.Length != 1 || string.IsNullOrWhiteSpace(DelSymbolField.StringValue))
				_PrintStatus(ErrorStatusMessages.INVALID_SYMBOL, StatusType.Error);
			else
				OnDelSymbol(DelSymbolField.StringValue[0]);
        }

        partial void ZeroSymbolPopUpButtonAction(Foundation.NSObject sender)
        {
            _tm.ZeroCharacter = ZeroSymbolPopUpButton.SelectedItem.Title[0];
            _TryToLoadInputStringWithInitialHeadPosition();
        }

        // Program Table Section --------------------------------------
        public void OnNewStatementAction(object sender, EventArgs e)
        {
            var btn = sender as TMStatementAddButton;

            if (btn == null)
                throw new Exception("New Statemnt event sender is not event button");

            OnNewStatement(btn.OldState, btn.OldSymbol);
        }

        public void OnModifyStatementNewStateAction(object sender, EventArgs e)
        {
			var popUp = sender as TMPopUpButton;

			if (popUp == null)
				throw new Exception("Not popup");
            
            var stmt = _tm.Program.StatementForOldStateAndSymbol(popUp.OldState, popUp.OldSymbol);

            if (popUp.SelectedItem.Title == _tm.Program.FinalState.Name)
                stmt.NewState = _tm.Program.FinalState;
            else
                stmt.NewState = _states[popUp.SelectedItem.Title];
        }

        public void OnModifyStatementNewSymbolAction(object sender, EventArgs e)
        {
			var popUp = sender as TMPopUpButton;

			if (popUp == null)
				throw new Exception("Not popup");

			var stmt = _tm.Program.StatementForOldStateAndSymbol(popUp.OldState, popUp.OldSymbol);
            stmt.NewSymbol = popUp.SelectedItem.Title[0];
        }

        public void OnModifyStatementDirectionAction(object sender, EventArgs e)
        {
			var popUp = sender as TMPopUpButton;

			if (popUp == null)
				throw new Exception("Not popup");

			var stmt = _tm.Program.StatementForOldStateAndSymbol(popUp.OldState, popUp.OldSymbol);
            stmt.Direction = popUp.SelectedItem.Title.ToStepDirection();
        }

        partial void ClearStatementsButtonAction(Foundation.NSObject sender)
        {
            _tm.EditableProgram.Statements.Clear();
            ProgramTableView.ReloadData();

            _UpdateSimulatorState();
        }

        // Infinite Loop Issue Adressing Section ----------------------
		partial void StepsLimitFieldAction(NSObject sender)
		{
            OnSetStepsLimit(StepsLimitField.StringValue);
		}
	
        // Speed
        partial void SpeedSliderAction(Foundation.NSObject sender)
        {
            _tm.Delay = 705 - SpeedSliderControl.IntValue;
        }

        // Predefined Tasks
        partial void Task1ButtonAction(Foundation.NSObject sender)
        {
            _LoadProgram(PredefinedPrograms.Task1, PredefinedPrograms.InputStrings.Task1);
        }

        partial void Task2ButtonAction(Foundation.NSObject sender)
        {
            _LoadProgram(PredefinedPrograms.Task2, PredefinedPrograms.InputStrings.Task2);
        }

        partial void Task3ButtonAction(Foundation.NSObject sender)
        {
            _LoadProgram(PredefinedPrograms.Task3, PredefinedPrograms.InputStrings.Task3, stepsLimit: 150);
        }

        partial void Task4ButtonAction(Foundation.NSObject sender)
        {
            _LoadProgram(PredefinedPrograms.Task4, PredefinedPrograms.InputStrings.Task4);
        }
    }
}
