﻿using System;
using System.Text.RegularExpressions;

using AppKit;

using TMCore;
using TMCore.Interfaces;
using TMCore.Exceptions;
using TMCore.Enums;

using TMUserInterface.StatusMessages;

namespace TMUserInterface
{
    partial class MainViewController
    {
        void OnInputStringAction(string inputString)
        {
			_simulatorElementsStates.InputStringOK = false;

            if (!Regex.IsMatch(inputString, @"^\S*$"))
				_PrintStatus(ErrorStatusMessages.INVALID_INPUT_STRING, StatusType.Error);
            else
            {
                _inputString = inputString;
                _simulatorElementsStates.InputStringOK = true;
                _TryToLoadInputStringWithInitialHeadPosition();
            }

            _UpdateSimulatorState();
        }

		void OnInitialHeadPositionAction(string initialHeadPositionString)
		{
			_simulatorElementsStates.InitialHeadPositionOK = false;

			if (initialHeadPositionString.Length == 0)
			{
				_initialHeadPosition = 0;
				InitialHeadPositionField.StringValue = 1.ToString();
				_simulatorElementsStates.InitialHeadPositionOK = true;
                _TryToLoadInputStringWithInitialHeadPosition();
			}
            else if (!int.TryParse(initialHeadPositionString, out int initialHeadPosition))
                _PrintStatus(ErrorStatusMessages.INITIAL_HEAD_POSITION_IS_NVI, StatusType.Error);

            else if (initialHeadPosition < 1)
                _PrintStatus(ErrorStatusMessages.INVALID_INITIAL_HEAD_POSITION, StatusType.Error);

			else
			{
				_initialHeadPosition = initialHeadPosition - 1;
				_simulatorElementsStates.InitialHeadPositionOK = true;

                _TryToLoadInputStringWithInitialHeadPosition();
			}

            _UpdateSimulatorState();
		}

        void _TryToLoadInputStringWithInitialHeadPosition()
        {
            _simulatorElementsStates.InputStringLoaded = false;

            if (!_simulatorElementsStates.InputStringOK ||
                !_simulatorElementsStates.InitialHeadPositionOK)
            {
                _AdjustUIToSimulatorState();
				return;
            }

            if (_inputString.Length != 0 && _initialHeadPosition >= _inputString.Length)
                _PrintStatus(ErrorStatusMessages.INITIAL_HEAD_POSITION_TOO_LARGE, StatusType.Error);

            else {
				try
				{
					_tm.ResetWithInputString(
						_inputString,
						_inputString.Length > 0 ? _initialHeadPosition : 0
					);
				}
				catch (TMachineException e)
				{
					_PrintStatus(e.Message, StatusType.Error);
					return;
				}

                _simulatorElementsStates.InputStringLoaded = true;
                _UpdateSimulatorState();
                _AdjustUIToSimulatorState();
                _UpdateTMStateData();
				_ClearLog();

				_PrintLineToLog(string.Format("0)\t--------\t\t{0}\t\t{1}", _tm.CurrentState, _tm.TapeRepresentation));

                _PrintStatus(
                    SuccessStatusMessages
                      .InputStringLoadedWithInitialHeadPosition(_inputString, _initialHeadPosition+1),
                    StatusType.Success);
            }
        }

        void OnInputStringClearAction()
        {
            InputStringField.StringValue = string.Empty;
            OnInputStringAction(string.Empty);
            _PrintStatus(SuccessStatusMessages.INPUT_STRING_CLEARED, StatusType.Success);
        }

        void OnPause()
        {
			try
			{
				_tm.Pause();
			}
			catch (TMachineException e)
			{
				_PrintStatus(e.Message, StatusType.Error);
			}

			_simulatorState = TMSimulatorState.Paused;
        }

		// Turing Machine States Section ------------------------------
        void OnAddNewState(string name)
        {
            try {
				_states.AddWithName(name);
            } catch (TMachineException e) {
                _PrintStatus(e.Message, StatusType.Error);
                return;
            }

            _PrintStatus(SuccessStatusMessages.StateAdded(name), StatusType.Success);
        }

        void OnDelState(string name)
        {
			try {
                _states.RemoveStateWithName(name);
                for (int i = 0; i < _tm.EditableProgram.Statements.Count; i++)
                    if (_tm.EditableProgram.Statements[i].OldState.Name == name)
                        _tm.EditableProgram.Statements.RemoveAt(i);
			}
			catch (TMachineException e)
			{
				_PrintStatus(e.Message, StatusType.Error);
                return;
			}

            _UpdateSimulatorState();
            _PrintStatus(SuccessStatusMessages.StateDeleted(name), StatusType.Success);
        }

        void OnStatesListChanged(object sender, EventArgs e)
        {
            ProgramTableView.ReloadData();
            InitialStatePopUpButton.Menu.RemoveAllItems();
            for (int i = 0; i < _states.Count; i++)
                InitialStatePopUpButton.Menu.AddItem(new NSMenuItem(_states[i].Name));
        }

        void OnSetInitialState(string initialStateName)
        {
            var s = _states[initialStateName];

			_tm.EditableProgram.InitialState = s ?? throw new Exception("Unexistant initial state");
                

            _UpdateTMStateData();
        }

        void OnSetFinalState(string finalStateName)
        {
            _tm.EditableProgram.FinalState.Name = finalStateName;
            ProgramTableView.ReloadData();

            _PrintStatus(SuccessStatusMessages.FinalStateSetTo(finalStateName), StatusType.Success);
        }


        // Alphabet Section ----------------------------------------------------
        void OnAddNewSymbol(char symbol)
        {
            try {
				_symbols.Add(symbol);
            } catch (TMachineException e) {
                _PrintStatus(e.Message, StatusType.Error);
                return;
            }

            _PrintStatus(SuccessStatusMessages.NewSymbolAdded(symbol), StatusType.Success);
        }

        void OnDelSymbol(char symbol)
        {
            if (_tm.ZeroCharacter == symbol) {
				_PrintStatus(ErrorStatusMessages.CANT_DELETE_ZERO_SYMBOL, StatusType.Error);
                return;
            }

    		try {
				_symbols.Remove(symbol);
			} catch (TMachineException e) {
				_PrintStatus(e.Message, StatusType.Error);
                return;
			}

            _PrintStatus(SuccessStatusMessages.SymbolDeleted(symbol), StatusType.Success);
        }

        void OnAlphabetChanged(object sender, TMSymbols.AlphabetChangedEventArgs e)
        {

            ZeroSymbolPopUpButton.Menu.RemoveAllItems();
            for (int i = 0; i < _symbols.Count; i++)
                ZeroSymbolPopUpButton.Menu.AddItem(new NSMenuItem(_symbols[i].ToString()));

            if (e.added != null)
                foreach (var s in e.added)
	            {
	                var col = new NSTableColumn("StatementsColumn");
	                col.Title = s.ToString();
                    col.HeaderCell.Alignment = NSTextAlignment.Center;
                    col.Width = 80;

					ProgramTableView.AddColumn(col);
	            }
            else
                foreach (var s in e.removed)
                {
                    foreach (var c in ProgramTableView.TableColumns())
                        if (c.Title == s.ToString())
                            ProgramTableView.RemoveColumn(c);
                }

            ProgramTableView.ReloadData();
        }

        // Program Table Section -----------------------------------------------
        void OnNewStatement(ITMState oldState, char oldSymbol)
        {
            var p = _tm.EditableProgram;
            var zc = _tm.ZeroCharacter;

            p.Statements.Add(new TMStatement(oldState, oldSymbol, p.FinalState, zc, StepDirection.Still));
            ProgramTableView.ReloadData();
            _UpdateSimulatorState();
        }


		// Infinite Loop Issue Adressing section -------------------------------
		void OnSetStepsLimit(string stepsLimitString)
		{
            _simulatorElementsStates.StepsLimitOK = false;

			if (stepsLimitString.Length == 0)
			{
				_stepsLimit = 0;
				_simulatorElementsStates.StepsLimitOK = true;
                StepsLimitField.StringValue = 0.ToString();
			}

            else if (!int.TryParse(stepsLimitString, out int stepsLimit) || stepsLimit < 0)
                _PrintStatus(ErrorStatusMessages.INVALIT_STEPS_LIMIT, StatusType.Error);
            
            else
            {
				_stepsLimit = stepsLimit;
                _simulatorElementsStates.StepsLimitOK = true;
				
				if (_stepsLimit == 0)
					_PrintStatus(SuccessStatusMessages.STEPS_LIMIT_SET_TO_ZERO, StatusType.Success);
				else
					_PrintStatus(SuccessStatusMessages.StepsLimitSetToNumber(stepsLimit), StatusType.Success);
            }

            _UpdateSimulatorState();
		}
    }
}
