﻿using AppKit;

using TMUserInterface.Extensions;

namespace TMUserInterface
{
    partial class MainViewController
    {
        void _UpdateSimulatorState()
        {
            bool ready = _simulatorElementsStates.InputStringOK &&
                _simulatorElementsStates.InitialHeadPositionOK &&
                _simulatorElementsStates.InputStringLoaded &&
                _simulatorElementsStates.StepsLimitOK &&
                _tm.Program.IsReady;
            
            if (_simulatorState != TMSimulatorState.Ready && ready)
                _simulatorState = TMSimulatorState.Ready;
            
            else if (_simulatorState != TMSimulatorState.NotReady && !ready)
                _simulatorState = TMSimulatorState.NotReady;
        }

        void _AdjustUIToSimulatorState()
		{
            if (!_simulatorElementsStates.InputStringOK)
                InputStringField.BackgroundColor = NSColor.FromRgb(255, 102, 102);
            else
                InputStringField.BackgroundColor = NSColor.White;

            if (!_simulatorElementsStates.InitialHeadPositionOK ||
                _simulatorElementsStates.InputStringOK && !_simulatorElementsStates.InputStringLoaded)
                InitialHeadPositionField.BackgroundColor = NSColor.FromRgb(255, 102, 102);
            else
                InitialHeadPositionField.BackgroundColor = NSColor.White;

            if (!_simulatorElementsStates.StepsLimitOK)
                StepsLimitField.BackgroundColor = NSColor.FromRgb(255, 102, 102);
            else
                StepsLimitField.BackgroundColor = NSColor.White;

			switch (_simulatorState)
            {
				case TMSimulatorState.NotReady: _AdjustUIToNotReady(); break;
                case TMSimulatorState.Ready: _AdjustUIToReady(); break;
                case TMSimulatorState.Executing: _AdjustUIToExecuting(); break;
                case TMSimulatorState.Paused: _AdjustUIToPaused(); break;
                case TMSimulatorState.Finished: _AdjustUIToFinished(); break;
			}
		}

        void _AdjustUIToNotReady()
		{
            InputStringField.Enabled = true;
            InitialHeadPositionField.Enabled = true;

            RunButton.Enabled = false;
            PauseButton.Enabled = false;
            ResetButton.Enabled = true;
            ClearButton.Enabled = false;
            NextButton.Enabled = false;

            AddNewStateField.Enabled = true;
            AddNewStateButton.Enabled = true;
			DelStateField.Enabled = true;
			DelStateButton.Enabled = true;
            InitialStatePopUpButton.Enabled = true;
            FinalStateField.Enabled = true;

            AddNewSymbolField.Enabled = true;
            AddNewSymbolButton.Enabled = true;
            DelSymbolField.Enabled = true;
            DelSymbolButton.Enabled = true;
            ZeroSymbolPopUpButton.Enabled = true;

            ProgramTableView.EnableStatements();
            ClearStatementsButton.Enabled = true;

            StepsLimitField.Enabled = true;
		}

        void _AdjustUIToReady()
        {
			InputStringField.Enabled = true;
			InitialHeadPositionField.Enabled = true;

            RunButton.Enabled = true;
			PauseButton.Enabled = false;
			ResetButton.Enabled = true;
            ClearButton.Enabled = true;
            NextButton.Enabled = true;

			AddNewStateField.Enabled = true;
			AddNewStateButton.Enabled = true;
            DelStateField.Enabled = true;
            DelStateButton.Enabled = true;
            InitialStatePopUpButton.Enabled = true;
            FinalStateField.Enabled = true;

			AddNewSymbolField.Enabled = true;
			AddNewSymbolButton.Enabled = true;
			DelSymbolField.Enabled = true;
			DelSymbolButton.Enabled = true;
			ZeroSymbolPopUpButton.Enabled = true;

            ProgramTableView.EnableStatements();
            ClearStatementsButton.Enabled = true;

            StepsLimitField.Enabled = true;
        }

        void _AdjustUIToExecuting()
        {
            InputStringField.Enabled = false;
            InitialHeadPositionField.Enabled = false;

            RunButton.Enabled = false;
			PauseButton.Enabled = true;
            ResetButton.Enabled = false;
            ClearButton.Enabled = false;
            NextButton.Enabled = false;

            AddNewStateField.Enabled = false;
            AddNewStateButton.Enabled = false;
            DelStateField.Enabled = false;
            DelStateButton.Enabled = false;
            InitialStatePopUpButton.Enabled = false;
            FinalStateField.Enabled = false;

			AddNewSymbolField.Enabled = false;
			AddNewSymbolButton.Enabled = false;
			DelSymbolField.Enabled = false;
			DelSymbolButton.Enabled = false;
			ZeroSymbolPopUpButton.Enabled = false;

            ProgramTableView.DisableStatements();
            ClearStatementsButton.Enabled = false;

            StepsLimitField.Enabled = false;
        }

        void _AdjustUIToPaused()
        {
			InputStringField.Enabled = true;
			InitialHeadPositionField.Enabled = true;

			RunButton.Enabled = true;
			PauseButton.Enabled = false;
            ResetButton.Enabled = true;
			ClearButton.Enabled = true;
			NextButton.Enabled = true;

			AddNewStateField.Enabled = true;
			AddNewStateButton.Enabled = true;
            DelStateField.Enabled = true;
			DelStateButton.Enabled = true;
            InitialStatePopUpButton.Enabled = true;
            FinalStateField.Enabled = true;

			AddNewSymbolField.Enabled = true;
			AddNewSymbolButton.Enabled = true;
			DelSymbolField.Enabled = true;
			DelSymbolButton.Enabled = true;
            ZeroSymbolPopUpButton.Enabled = false;

            ProgramTableView.EnableStatements();
            ClearStatementsButton.Enabled = true;

            StepsLimitField.Enabled = true;
        }

        void _AdjustUIToFinished()
        {
			InputStringField.Enabled = true;
			InitialHeadPositionField.Enabled = true;

            RunButton.Enabled = false;
            PauseButton.Enabled = false;
            ResetButton.Enabled = true;
			ClearButton.Enabled = true;
            NextButton.Enabled = false;

			AddNewStateField.Enabled = true;
			AddNewStateButton.Enabled = true;
			DelStateField.Enabled = true;
			DelStateButton.Enabled = true;
            InitialStatePopUpButton.Enabled = true;
            FinalStateField.Enabled = true;

			AddNewSymbolField.Enabled = true;
			AddNewSymbolButton.Enabled = true;
			DelSymbolField.Enabled = true;
			DelSymbolButton.Enabled = true;
            ZeroSymbolPopUpButton.Enabled = false;

            ProgramTableView.EnableStatements();
            ClearStatementsButton.Enabled = true;

            StepsLimitField.Enabled = true;
        }
    }
}
